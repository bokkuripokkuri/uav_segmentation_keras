
import numpy as np
import matplotlib.pyplot as plt



# helper function for data visualization
def visualize_save(**images):
    """PLot images in one row."""
    n = len(images)
    plt.figure(figsize=(16, 5))
    for i, (name, image) in enumerate(images.items()):
        plt.subplot(1, n, i + 1)
        plt.xticks([])
        plt.yticks([])
        plt.title(' '.join(name.split('_')).title())

        # if image.shape[-1]>3:
        #     image=np.argmax(image, axis=2)

        plt.imshow(image)
        plt.savefig("./org_msk.png")
        plt.clf()
        plt.cla()
    # plt.show()
    
# helper function for data visualization
def visualize(**images):
    """PLot images in one row."""
    n = len(images)
    plt.figure(figsize=(16, 5))
    for i, (name, image) in enumerate(images.items()):
        plt.subplot(1, n, i + 1)
        plt.xticks([])
        plt.yticks([])
        plt.title(' '.join(name.split('_')).title())

        # if image.shape[-1]>3:
        #     image=np.argmax(image, axis=2)

        plt.imshow(image)
    plt.show()


def pltHistRGB(img_array):
    
    red = img_array[:,:,0].flatten()
    green = img_array[:,:,1].flatten()
    blue = img_array[:,:,2].flatten()

    labels = ['Red', 'Green', 'Blue']

    # 下段のヒストグラムの設定
    plt.hist([red, green, blue],
            bins=np.arange(256 + 1),
            label=labels,
            color = labels,
            range = (0,255),
            # yrange = (0, 256*256)
    )
    plt.legend()

import matplotlib.gridspec as gridspec

    
    
def pltHist3band(img_array,titleStr):
    img_array = ((img_array-0)*255).astype(np.uint8)

    red = img_array[:,:,0].flatten()
    green = img_array[:,:,1].flatten()
    blue = img_array[:,:,2].flatten()

    figWidth = 4
    figHeight = figWidth*2
    plt.figure(figsize=(figWidth, figHeight))
    plt.title(titleStr)

    # plt.subplot(row, column, figre setNum)
    # 上段の画像表示の設定
    plt.subplot(2, 1, 1)
    plt.imshow(img_array)
    # plt.axis('off')

    labels = ['Red', 'Green', 'Blue']

    # 下段のヒストグラムの設定
    plt.subplot(2, 1, 2) 
    plt.hist([red, green, blue],
            bins=np.arange(256 + 1),
            label=labels,
            color = labels,
            range = (0,255),
            # yrange = (0, 256*256)
    )
    plt.legend()



def pltOrgMskHist(org,msk):
    # sepalete 2*9 block
    gs = gridspec.GridSpec(2,9)

    org = np.array(org)
    msk = np.array(msk)

    plt.subplot(2,3,1)
    plt.title("Original Image")
    plt.imshow(org)
    plt.axis('off')

    plt.subplot(2,3,2)
    plt.title("Label")
    plt.imshow(msk)
    plt.axis('off')

    plt.subplot(2,3,3)
    plt.title("Image Normarize")
    # orgNorm = (pre.denormalize(org)*255).astype(np.uint8)
    orgNorm = (pre.band2Norm(org)*255).astype(np.uint8)
    plt.imshow(orgNorm)
    plt.axis('off')

    plt.subplot(gs[1,0:4])
    plt.title("Original")
    pltHistRGB(org)
    plt.subplot(gs[1,5:])
    plt.title("Normarized")
    pltHistRGB(orgNorm)
