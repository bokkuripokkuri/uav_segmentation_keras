# !ln -sf /opt/bin/nvidia-smi /usr/bin/nvidia-smi
# !pip install gputil
# !pip install psutil
# !pip install humanize
import psutil
import humanize
import os, sys
import GPUtil as GPU
# gpu = GPU.getGPUs()

# def printMemory():
#     print("--------RAM-Check-----------")
#     process = psutil.Process(os.getpid())
#     print("Gen RAM Free: " + humanize.naturalsize( psutil.virtual_memory().available ), " | Proc size: " + humanize.naturalsize( process.memory_info().rss))
# #   print("GPU RAM Free: {0:.0f}MB | Used: {1:.0f}MB | Util {2:3.0f}% | Total {3:.0f}MB".format(gpu.memoryFree, gpu.memoryUsed, gpu.memoryUtil*100, gpu.memoryTotal))
#     print("--------RAM-Check-----------")

# import GPUtil
# GPUtil.showUtilization(all=False, attrList=None, useOldCode=False)

import Func_Unet_Satelite as mf

import subprocess
import json

DEFAULT_ATTRIBUTES = (
    'index',
    'uuid',
    'name',
    'timestamp',
    'memory.total',
    'memory.free',
    'memory.used',
    'utilization.gpu',
    'utilization.memory'
)

def get_gpu_info(nvidia_smi_path='nvidia-smi', keys=DEFAULT_ATTRIBUTES, no_units=True):
    nu_opt = '' if not no_units else ',nounits'
    cmd = '%s --query-gpu=%s --format=csv,noheader%s' % (nvidia_smi_path, ','.join(keys), nu_opt)
    output = subprocess.check_output(cmd, shell=True)
    lines = output.decode().split('\n')
    lines = [ line.strip() for line in lines if line.strip() != '' ]

    GPUtil.showUtilization(all=False, attrList=None, useOldCode=False)

    return [ { k: v for k, v in zip(keys, line.split(', ')) } for line in lines ]

# import pprint
# pprint.pprint(get_gpu_info())
import tensorflow as tf
from keras import backend as K
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
K.set_session(sess)


import os
os.environ['CUDA_VISIBLE_DEVICES'] = '0'

import cv2
import keras
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

import paramLoader




BATCH_SIZE, imgSize, trainingSize, DATASET_DIR, LR, EPOCHS = paramLoader.getConfigValue("./config_seg.ini")
# BATCH_SIZE, imgSize, DATASET_DIR, LR, EPOCHS = paramLoader.getConfigValue("./config_seg_test.ini")
Rasvalue, JusyuStr, CLASSES, dicelossWeights, dataRatio, JusyuCodes = paramLoader.getJsonValue("./JusyuNames.json")
trainRatio, validRatio, testRatio = dataRatio.values()


from datetime import timedelta, timezone
from datetime import datetime 

def generate_dir_name():
    # タイムゾーンの生成
    JST = timezone(timedelta(hours=+9), 'JST')
    # GOOD, タイムゾーンを指定している．早い       
    datetime_dat = datetime.now()
    time_str = datetime_dat.strftime("%Y%m%d_%H%M") 
    return time_str #datetime.datetime.today().strftime("%Y%m%d_%H%M")

# resultDir = "./result/" + generate_dir_name() + "_Batch" + str(BATCH_SIZE) + "_imgSize" + str(imgSize) + "_Epoch" + str(EPOCHS) + "/"
# os.makedirs(resultDir,exist_ok=True)


import glob
from tqdm import tqdm

def ckFileNamesMsk(orgFilePaths):
    notFindList = []
    for orgPath in tqdm(glob.glob(orgFilePaths + "/*")):
        mskPath = orgPath.replace("org","msk").replace("jpg","png")
        if not os.path.exists(mskPath):
            # print(orgPath)
            notFindList.append(orgPath)
            os.remove(orgPath)
        # print(mskPath)
    return notFindList



# classes for data loading and preprocessing
class DatasetDict:
    """CamVid Dataset. Read images, apply augmentation and preprocessing transformations.
    Args:
        images_dir (str): org images dict
        masks_dir (str):  msk images dict
        class_values (list): values of classes to extract from segmentation mask
        augmentation (albumentations.Compose): data transfromation pipeline 
            (e.g. flip, scale, etc.)
        preprocessing (albumentations.Compose): data preprocessing 
            (e.g. noralization, shape manipulation, etc.)
    
    """

    def __init__(
            self, 
            images_dict, 
            masks_dict, 
            classes=None, 
            augmentation=None, 
            preprocessing=None,
    ):
        # self.ids = os.listdir(images_dir)
        self.ids = list(images_dict.keys())

        self.images_fps = images_dict
        self.masks_fps = masks_dict
        
        # print("classes:",classes)

        # convert str names to class values on masks
        self.class_values = [i for i, cls in enumerate(classes)]# self.class_values = [self.classes.index(cls.lower()) for cls in classes]
       
        self.augmentation = augmentation
        self.preprocessing = preprocessing
    
    # python class リストのようにindexで取り出す際の処理 
    def __getitem__(self, i):
    
        # print(self.images_fps[i])
    
        # read data
        image = self.images_fps[self.ids[i]]
        image = np.array(image)

        mask = self.masks_fps[self.ids[i]]
        mask = np.array(mask)
        
        # extract certain classes from mask (e.g. cars)
        masks = [(mask == v) for v in self.class_values]
        # type(masks):List   masks.shape => (len(Classes), imgSize, imgSize) (6, 1024, 1024)

        # print("masks Value",np.unique(masks))

        mask = np.stack(masks, axis=-1).astype('float')
        # masks.shape => (imgSize, imgSize, len(Classes)) (1024, 1024, 6)
        
        # add background if mask is not binary
        if mask.shape[-1] != 1:
            background = 1 - mask.sum(axis=-1, keepdims=True)
            mask = np.concatenate((mask, background), axis=-1)
        
        # apply augmentations
        if self.augmentation:
            sample = self.augmentation(image=image, mask=mask)
            image, mask = sample['image'], sample['mask']
        
        # apply preprocessing
        if self.preprocessing:
            sample = self.preprocessing(image=image, mask=mask)
            image, mask = sample['image'], sample['mask']
        
        # print("image",image.shape,image.dtype,np.max(image),np.min(image),np.mean(image))
        return image, mask
        
    def __len__(self):
        return len(self.ids)


# classes for data loading and preprocessing
class Dataset:
    """CamVid Dataset. Read images, apply augmentation and preprocessing transformations.
    
    Args:
        images_dir (str): path to images folder
        masks_dir (str): path to segmentation masks folder
        class_values (list): values of classes to extract from segmentation mask
        augmentation (albumentations.Compose): data transfromation pipeline 
            (e.g. flip, scale, etc.)
        preprocessing (albumentations.Compose): data preprocessing 
            (e.g. noralization, shape manipulation, etc.)
    
    """


    # JusyuName={"スギ":"ceder","ヒノキ":"cypress","マツ":"matsu","その他針葉樹":"conifer","竹林":"bamboo","広葉樹":"hardwood","その他":"nonforest","ALL":"all"}
    JusyuName={"スギ":"ceder","ヒノキ":"cypress","マツ":"matsu","その他針葉樹":"conifer","マダケ":"madake","モウソウチク":"mousou","竹林":"bamboo","広葉樹":"hardwood"}
    # JusyuName = {"針葉樹":"conifer","竹林":"bamboo","広葉樹":"hardwood","その他":"nonforest"}
    CLASSES = list(JusyuName.values())

    class_values = CLASSES

    def __init__(
            self, 
            images_dir, 
            masks_dir, 
            classes=None, 
            augmentation=None, 
            preprocessing=None,
    ):
        self.ids = os.listdir(Fimages_dir)
        self.images_fps = [os.path.join(images_dir, image_id) for image_id in self.ids]
        self.masks_fps = [os.path.join(masks_dir, image_id) for image_id in self.ids]
        
        # print("classes:",classes)

        # convert str names to class values on masks
        self.class_values = [self.CLASSES.index(cls.lower()) for cls in classes]
        
        self.augmentation = augmentation
        self.preprocessing = preprocessing
    
    def __getitem__(self, i):
        

        
        # read data
        image = cv2.imread(self.images_fps[i])
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        image = (denormalize(image)*255).astype(np.uint8)

        if os.path.exists(self.masks_fps[i]):
            mask = Image.open(self.masks_fps[i])        
        else:
            mask = Image.open(self.masks_fps[i].replace("jpg","png"))
        mask = np.array(mask)

        # mask = cv2.imread(self.masks_fps[i], 0)

        # print("mask Value",np.unique(mask))

        # extract certain classes from mask (e.g. cars)
        masks = [(mask == v) for v in self.class_values]

        # print("masks Value",np.unique(masks))


        mask = np.stack(masks, axis=-1).astype('float')
        
        # add background if mask is not binary
        if mask.shape[-1] != 1:
            background = 1 - mask.sum(axis=-1, keepdims=True)
            mask = np.concatenate((mask, background), axis=-1)
        
        # apply augmentations
        if self.augmentation:
            sample = self.augmentation(image=image, mask=mask)
            image, mask = sample['image'], sample['mask']
        
        # apply preprocessing
        if self.preprocessing:
            sample = self.preprocessing(image=image, mask=mask)
            image, mask = sample['image'], sample['mask']
        
        # print("image",image.shape,image.dtype,np.max(image),np.min(image),np.mean(image))
        return image, mask
        
    def __len__(self):
        return len(self.ids)
    
import preprocess as pre
    
def saveDataset(dataset, saveDir):
    counter = 0
    print("Save Dataset")
    for org, msk in tqdm(dataset):
        savePath_org = saveDir + str(counter) + ".bmp"
        savePath_msk = savePath_org.replace("org","msk")
        os.makedirs(os.path.dirname(savePath_org),exist_ok=True)
        os.makedirs(os.path.dirname(savePath_msk),exist_ok=True)

        org = (pre.simpleNorm(org) * 255).astype(np.uint8)

        cv2.imwrite(savePath_org,org)
        cv2.imwrite(savePath_msk,np.argmax(msk,axis=-1))
        counter += 1


class Dataloder(keras.utils.Sequence):
    """Load data from dataset and form batches
    
    Args:
        dataset: instance of Dataset class for image loading and preprocessing.
        batch_size: Integet number of images in batch.
        shuffle: Boolean, if `True` shuffle image indexes each epoch.
    """
    
    def __init__(self, dataset, batch_size=1, shuffle=False):
        self.dataset = dataset
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.indexes = np.arange(len(dataset))

        self.on_epoch_end()

    def __getitem__(self, i):
        
        # collect batch data
        start = i * self.batch_size
        stop = (i + 1) * self.batch_size
        data = []
        for j in range(start, stop):
            data.append(self.dataset[j])
        
        # transpose list of lists
        batch = [np.stack(samples, axis=0) for samples in zip(*data)]
        
        return batch
    
    def __len__(self):
        """Denotes the number of batches per epoch"""
        return len(self.indexes) // self.batch_size
    
    def on_epoch_end(self):
        """Callback function to shuffle indexes each epoch"""
        if self.shuffle:
            self.indexes = np.random.permutation(self.indexes)   



import albumentations as A

def round_clip_0_1(x, **kwargs):
    return x.round().clip(0, 1)

# define heavy augmentations
def get_training_augmentation(imgSize):
    train_transform = [

        # A.HorizontalFlip(p=0.5),

        A.ShiftScaleRotate(scale_limit=0.5, rotate_limit=0, shift_limit=0.1, p=1, border_mode=0),

        # A.PadIfNeeded(min_height=imgSize, min_width=imgSize, always_apply=True, border_mode=0),
        # A.RandomCrop(height=imgSize, width=imgSize, always_apply=True),

        A.IAAAdditiveGaussianNoise(p=0.2),
        A.IAAPerspective(p=0.5),

        A.OneOf(
            [
                A.CLAHE(p=1),
                A.RandomBrightness(p=1),
                A.RandomGamma(p=1),
            ],
            p=0.9,
        ),

        A.OneOf(
            [
                A.IAASharpen(p=1),
                A.Blur(blur_limit=3, p=1),
                A.MotionBlur(blur_limit=3, p=1),
            ],
            p=0.9,
        ),

        A.OneOf(
            [
                A.RandomContrast(p=1),
                A.HueSaturationValue(p=1),
            ],
            p=0.9,
        ),
        A.Lambda(mask=round_clip_0_1)
    ]
    return A.Compose(train_transform)


def get_validation_augmentation(imgSize):
    """Add paddings to make image shape divisible by 32"""
    test_transform = [
        A.PadIfNeeded(imgSize, imgSize)
    ]
    return A.Compose(test_transform)

def get_preprocessing(preprocessing_fn):
    """Construct preprocessing transform
    
    Args:
        preprocessing_fn (callbale): data normalization function 
            (can be specific for each pretrained neural network)
    Return:
        transform: albumentations.Compose
    
    """
    
    _transform = [
        A.Lambda(image=preprocessing_fn),
    ]
    return A.Compose(_transform)



def orgPredict(org_np,model):
    prd = model.predict(np.expand_dims(org_np, 0))

    prd_arg = np.argmax(prd, axis=-1)
    
    # numpyで0-1のマスク画像を0-255に変換
    prd_sqe= prd_arg.squeeze()
    
    return prd_sqe


def convMskPrd_forShow(msk):
    return np.stack((msk,)*3, axis=-1)




def smUnet(CLASSES:list,diceLossWeights:list, BACKBONE:str):
   
 
#     if not len(CLASSES)+1 == len(diceLossWeights):
#         print("Not match CLASSES+1:", len(CLASSES)+1, "diceLossWeights", len(diceLossWeights))
#         return None
    import segmentation_models as sm

    # segmentation_models could also use `tf.keras` if you do not have Keras installed
    # or you could switch to other framework using `sm.set_framework('tf.keras')`


    # BACKBONE = 'efficientnetb3'
    preprocess_input = sm.get_preprocessing(BACKBONE)
    # pprint.pprint(get_gpu_info())



    # define network parameters
    n_classes = 1 if len(CLASSES) == 1 else (len(CLASSES) + 1)  # case for binary and multiclass segmentation
    activation = 'sigmoid' if n_classes == 1 else 'softmax'



    print("U-net Model")

    #create model
    model = sm.Unet(BACKBONE, classes=n_classes, activation=activation)
    # pprint.pprint(get_gpu_info())



    # define optomizer
    optim = keras.optimizers.Adam(LR)


    # Segmentation models losses can be combined together by '+' and scaled by integer or float factor
    # set class weights for dice_loss (car: 1.; pedestrian: 2.; background: 0.5;)
    dice_loss = sm.losses.DiceLoss(class_weights=np.array(diceLossWeights)) 
    focal_loss = sm.losses.BinaryFocalLoss() if n_classes == 1 else sm.losses.CategoricalFocalLoss()
    total_loss = dice_loss + (1 * focal_loss)

    # actulally total_loss can be imported directly from library, above example just show you how to manipulate with losses
    # total_loss = sm.losses.binary_focal_dice_loss # or sm.losses.categorical_focal_dice_loss 

    metrics = [sm.metrics.IOUScore(threshold=0.5), sm.metrics.FScore(threshold=0.5)]

    # compile keras model with defined optimozer, loss and metrics
    model.compile(optim, total_loss, metrics)

    return model





# # Dataset for train images
# train_dataset = Dataset(
#     x_train_dir, 
#     y_train_dir, 
#     classes=CLASSES, 
#     augmentation=get_training_augmentation(),
#     preprocessing=get_preprocessing(preprocess_input),
# )

# # Dataset for validation images
# valid_dataset = Dataset(
#     x_valid_dir, 
#     y_valid_dir, 
#     classes=CLASSES, 
#     augmentation=get_validation_augmentation(),
#     preprocessing=get_preprocessing(preprocess_input),
# )



def showPrdMsk(org,msk,model,savePath):

    prd = orgPredict(org,model)
    print(np.unique(prd))
    # plt.figure(figsize=(2,6))

    plt.subplot(1,3,1)
    plt.imshow(org)
    plt.axis("off")
    plt.subplot(1,3,2)
    plt.imshow(prd)
    plt.axis("off")
    plt.subplot(1,3,3)
    plt.imshow(msk)
    plt.axis("off")
    plt.savefig(savePath)

def smTraining(train_dataset, valid_dataset, test_dataset, model, imgSize:int, BATCH_SIZE:int, n_classes:int, resultDir:str):


    train_dataloader = Dataloder(train_dataset, batch_size=BATCH_SIZE, shuffle=True)
    valid_dataloader = Dataloder(valid_dataset, batch_size=1, shuffle=False)

    print(train_dataloader[0][0].shape)
    print(train_dataloader[0][0].shape)


    # check shapes for errors
    # print("assert",train_dataloader[0][0].shape, (BATCH_SIZE, imgSize, imgSize, 3))
    assert train_dataloader[0][0].shape == (BATCH_SIZE, imgSize, imgSize, 3)
    assert train_dataloader[0][1].shape == (BATCH_SIZE, imgSize, imgSize, n_classes)


 
    def saveModel(model):
        
        #モデルと重みを出力する
        model.save_weights(resultDir + 'keras_model_weights' + '.h5')
        model.save(resultDir + 'keras_model' + '.hdf5')

        #モデルのアーキテクチャを保存 save as JSON
        json_string = model.to_json()
        open(os.path.join(resultDir + 'keras_model_Architecture' + '.json'), 'w').write(json_string)
        # print("モデルと重みを保存しました。", nowTimeStr, ParamStr)


    # import shutil
    # shutil.copy(__file__, resultDir + os.path.basename(__file__))

    # shutil.copy(config_ini_path, resultDir + os.path.basename(config_ini_path))

    # define callbacks for learning rate scheduling and best checkpoints saving



    def build_callbacks(): 

        checkpointer = keras.callbacks.ModelCheckpoint(resultDir + 'best_unet_model_imgSize' + str(imgSize) + '.h5',
                                                    save_weights_only=True,
                                                    save_best_only=True,
                                                    mode='min')

        ReduceLROnPlateau = keras.callbacks.ReduceLROnPlateau()
        
        csvLogger = keras.callbacks.CSVLogger(resultDir + 'csvLogger' + str(imgSize) + '.csv',separator=",")

        tensorBorder = keras.callbacks.TensorBoard(log_dir=resultDir + 'Tensorboard.logs',
                                                histogram_freq=0,
                                                batch_size=BATCH_SIZE,
                                                write_graph=True,
                                                write_grads=True,
                                                write_images=True,
                                                embeddings_freq=0,
                                                embeddings_layer_names=None,
                                                embeddings_metadata=None)

        callbacks = [checkpointer, ReduceLROnPlateau, PlotLearning(), csvLogger, tensorBorder]
            
        #作成済み重みを指定して読み込む
        # model.load_weights(workSpacePath + '02_output_coniferOnly_1024Size_20190906_1407/ModelWeight/unet_ModelCheckpoint.h5')
            
        print("build_callbacks")
        # printMemory()

        return callbacks


        
    # inheritance for training process plot 
    class PlotLearning(keras.callbacks.Callback):
        
        def on_train_begin(self, logs={}):
            print("on_train_begin")
            
            self.i = 0
            self.x = []
            self.losses = []
            self.val_losses = []
            self.acc = []
            self.val_acc = []
            self.fig = plt.figure()
            self.logs = []


            

            
        def on_epoch_end(self, epoch, logs={}):
            print("on_epoch_end")
            #pltの初期化
            # plt.figure(clear=True)
            # plt.clf()
            self.logs.append(logs)
            self.x.append(self.i)
            self.losses.append(logs.get('loss'))
            self.val_losses.append(logs.get('val_loss'))
            self.acc.append(logs.get('mean_iou'))
            self.val_acc.append(logs.get('val_mean_iou'))
            self.i += 1

            
            # print('\nIoU=',self.acc[-1],'val_IoU=',self.val_acc[-1])      
            # print('loss=',logs.get('loss'),'val_loss=',logs.get('val_loss'),'mean_iou=',logs.get('mean_iou'),'val_mean_iou=',logs.get('val_mean_iou'),"\n")



            # if self.i % 5 == 0:
            #     imgIdx = np.random.randint(0,len(test_dataset)) 
            #     org, msk = test_dataset[imgIdx]
                
            #     showPrdMsk(org,msk,model, resultDir + "Predicted_Image/" + str(imgIdx)+".png")




    print("Training Model Unet")
    # train model
    history = model.fit_generator(
        train_dataloader, 
        steps_per_epoch=len(train_dataloader), 
        epochs=EPOCHS, 
        callbacks=build_callbacks(), 
        validation_data=valid_dataloader, 
        validation_steps=len(valid_dataloader),
    )
    # pprint.pprint(get_gpu_info())

    return history


  
def plotHistory(history, saveDir:str):

    # Plot training & validation iou_score values
    plt.figure(figsize=(30, 5))
    plt.subplot(121)
    plt.plot(history.history['iou_score'])
    plt.plot(history.history['val_iou_score'])
    plt.title('Model iou_score')
    plt.ylabel('iou_score')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.savefig(saveDir + 'IoU_trainingHistry_unet_model' + '.png')

    # Plot training & validation loss values
    plt.subplot(122)
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.savefig(saveDir + 'Loss_trainingHistry_unet_model' + '.png')
    plt.show()

def saveHistory(history, saveDir:str):

    # Plot training & validation iou_score values
    plt.figure(figsize=(30, 5))
    plt.subplot(121)
    plt.plot(history.history['iou_score'])
    plt.plot(history.history['val_iou_score'])
    plt.title('Model iou_score')
    plt.ylabel('iou_score')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.savefig(saveDir + 'IoU_trainingHistry_unet_model' + '.png')

    # Plot training & validation loss values
    plt.subplot(122)
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.savefig(saveDir + 'Loss_trainingHistry_unet_model' + '.png')
    # plt.show()


def evaluateModel(model,test_dataset,metrics):

    test_dataloader = Dataloder(test_dataset, batch_size=1, shuffle=False)
    scores = model.evaluate_generator(test_dataloader)

    print("Loss: {:.5}".format(scores[0]))
    for metric, value in zip(metrics, scores[1:]):
        print("mean {}: {:.5}".format(metric.__name__, value))


def saveModel(model, resultDir):

    #モデルと重みを出力する
    model.save_weights(resultDir + 'Model_unet_weights' + '.h5')
    model.save(resultDir +'Model_unet' + '.hdf5')
    model.save(resultDir +'Model_unet' + '_includeOptimizerFalse.hdf5', include_optimizer=False)
#     model = keras.models.load_model('model.h5', compile=False)

    json_string = model.to_json()
    open(resultDir + "Model_unet" + ".json", "w").write(json_string)

    yaml_string = model.to_yaml()
    open(resultDir + 'Model_unet' + '.yaml', 'w').write(yaml_string)    



# classes for data loading and preprocessing
class Dataset_pred:
    """CamVid Dataset. Read images, apply augmentation and preprocessing transformations.
    
    Args:
        images_dir (str): path to images folder
        masks_dir (str): path to segmentation masks folder
        class_values (list): values of classes to extract from segmentation mask
        augmentation (albumentations.Compose): data transfromation pipeline 
            (e.g. flip, scale, etc.)
        preprocessing (albumentations.Compose): data preprocessing 
            (e.g. noralization, shape manipulation, etc.)
    
    """


    JusyuName={"スギ":"ceder","ヒノキ":"cypress","マツ":"matsu","その他針葉樹":"conifer","竹林":"bamboo","広葉樹":"hardwood","その他":"nonforest","ALL":"all"}
    CLASSES = list(JusyuName.values())

    class_values = CLASSES

    def __init__(
            self, 
            images_dir, 
            classes=None, 
            preprocessing=None,
    ):
        self.ids = glob.glob(images_dir + "*.*g")

        self.images_fps = [image_id for image_id in self.ids]
        # self.images_fps = [os.path.join(images_dir, image_id) for image_id in self.ids]
        
        # print("classes:",classes)

        # convert str names to class values on masks
        self.class_values = [self.CLASSES.index(cls.lower()) for cls in classes]

        self.preprocessing = preprocessing
        
    
    def __getitem__(self, i):
        
        # read data
        image = Image.open(self.images_fps[i])
        image = np.array(image)


        # apply preprocessing
        if self.preprocessing:
            sample = self.preprocessing(image=image)
            image = sample['image'] 
        return image, self.images_fps[i]
        
    def __len__(self):
        return len(self.ids)





def PredImgConv(ordImgPath,prdNumpy):
    img_org = Image.open(ordImgPath)
    orgSize = img_org.size
    # print("prdNumpy:shape ",prdNumpy.shape)
    prd = Image.fromarray(np.uint8(prdNumpy))
    prd = prd.resize(orgSize)
    return prd

def resizeCk(Numpy,size):
    img = Image.fromarray(np.uint8(Numpy))
    img = img.resize((size,size))
    Numpy_resized = np.array(img)
    return Numpy_resized

def resizeNp(Numpy,size_taple):
    img = Image.fromarray(np.uint8(Numpy))
    img = img.resize(size_taple)
    Numpy_resized = np.array(img)
    return Numpy_resized





