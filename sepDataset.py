import os, glob, shutil, sys, random
from tqdm import tqdm
from PIL import Image
import cv2
import numpy as np



def separateDatasetDir(inDir:str, trainRatio:float,validRatio:float, testRatio:float):
    inDir_msk = inDir.replace("org","msk")

    filePaths = glob.glob(inDir + "*")  
    print("All Files",len(filePaths))

    if len(filePaths)==0:
        print("Not Find Files", inDir + "/*")
        # sys.exit()
    

    # saveDir = os.path.dirname(os.path.dirname(filePaths[0])) + "\\DatasetExtract\\" + os.path.dirname(filePaths[0]).split("\\")[-1] + "_" + str(int(ratio*100)).zfill(3) + "per_" + str(len(filePaths)) + "\\"
    saveDir_train = os.path.dirname(filePaths[0]) + "_train" + "_" + str(int(trainRatio*100)).zfill(3)
    saveDir_valid = os.path.dirname(filePaths[0]) + "_valid" + "_" + str(int(validRatio*100)).zfill(3)
    saveDir_test = os.path.dirname(filePaths[0]) + "_test" + "_" + str(int(testRatio*100)).zfill(3)


    os.makedirs(saveDir_train,exist_ok=True)
    os.makedirs(saveDir_valid,exist_ok=True)
    os.makedirs(saveDir_test,exist_ok=True)


    filePaths_shuffled = random.sample(filePaths, int(len(filePaths)))

    threshod1 = int(len(filePaths)*trainRatio)
    threshod2 = threshod1 + int(len(filePaths)*validRatio)
    threshodEnd = threshod2 + int(len(filePaths)*testRatio)

    print(threshod1,threshod2,threshodEnd)

    filePaths_shuffled_train = filePaths_shuffled[:threshod1]
    filePaths_shuffled_valid = filePaths_shuffled[threshod1:threshod2]
    filePaths_shuffled_test = filePaths_shuffled[threshod2:threshodEnd]


 
    copyOrgmsk(filePaths_shuffled_train,saveDir_train)
    copyOrgmsk(filePaths_shuffled_valid,saveDir_valid)
    copyOrgmsk(filePaths_shuffled_test,saveDir_test)

    print("\n","Copy", saveDir_train)
    print("Copy", saveDir_valid)
    print("Copy", saveDir_test)


    return saveDir_train, saveDir_valid, saveDir_test


import pickle

def pickeload(picklePath):
    with open(picklePath, 'rb') as pickle_file:
        Dict = pickle.load(pickle_file)
        return Dict

def separateDatasetDict(orgDict:dict, mskDict:dict, trainRatio:float,validRatio:float, testRatio:float):
    # inDictPath_msk = inDictPath.replace("org","msk")

    # orgDict = pickeload(inDictPath)
    # mskDict = pickeload(inDictPath_msk)

    # for key in list(orgDict.keys()):
    #     # print("org",orgDict[key].mode, "msk",mskDict[key].mode)

    print("All Files",len((orgDict.keys())))

    keysList = list(orgDict.keys())

    keysList_shuffled = random.sample(keysList, int(len(keysList)))

    threshod1 = int(len(keysList)*trainRatio)
    threshod2 = threshod1 + int(len(keysList)*validRatio)
    threshodEnd = threshod2 + int(len(keysList)*testRatio)

    print("0:",threshod1,":",threshod1 + threshod2,":",threshodEnd)

    filePaths_shuffled_train = keysList_shuffled[:threshod1]
    filePaths_shuffled_valid = keysList_shuffled[threshod1:threshod2]
    filePaths_shuffled_test = keysList_shuffled[threshod2:threshodEnd]


    def makeorgMskdict(keyslist, orgDict, mskDict):
        orgDict_new = {}
        mskDict_new = {}
        for key in keyslist:
            orgDict_new[key] = orgDict[key]
            mskDict_new[key] = mskDict[key]
        return orgDict_new, mskDict_new
    

    orgDict_train, mskDict_train = makeorgMskdict(filePaths_shuffled_train, orgDict, mskDict)
    orgDict_valid, mskDict_valid = makeorgMskdict(filePaths_shuffled_valid, orgDict, mskDict)
    orgDict_test, mskDict_test = makeorgMskdict(filePaths_shuffled_test, orgDict, mskDict)


    return orgDict_train, mskDict_train, orgDict_valid, mskDict_valid, orgDict_test, mskDict_test


# org????????????????????msk?????????????????????
def copyOrgmsk(filePathList_org:list, saveDir:str):

    saveDir_msk = saveDir.replace("org","msk")
    os.makedirs(saveDir_msk,exist_ok=True)

    for filePath in tqdm(filePathList_org):
        # filePath = os.path.join(os.path.dirname(filePath),os.path.basename(filePath))

        filePath_msk = glob.glob(os.path.dirname(filePath.replace("org","msk")) + "/" + os.path.basename(filePath).split(".")[0] + "*")

        if len(filePath_msk)==0:
            continue
        else:
            filePath_msk = filePath_msk[0]

        # Nodata 0 image is pass
        msk = np.array(Image.open(filePath_msk))
        if np.mean(msk) == 0.0:
            continue


        # copiedPath_org = os.path.join(saveDir,os.path.basename(filePath))
        copiedPath_org = saveDir + "/" + os.path.basename(filePath)[:-4] + ".jpg"
        # copiedPath_msk = os.path.join(saveDir_msk, os.path.basename(filePath_msk))
        copiedPath_msk = copiedPath_org.replace("org","msk")[:-4] + ".png"
        # print("copiedPath_org",copiedPath_org)
        if os.path.exists(filePath) and os.path.exists(filePath_msk):
            if not os.path.exists(copiedPath_org):
                shutil.copyfile(filePath, copiedPath_org)
                # print("Copied org",copiedPath_org)
            if not os.path.exists(copiedPath_msk):
                shutil.copyfile(filePath_msk, copiedPath_msk)
                # print("Copied msk",copiedPath_org)


# inDir = "./dataset_Seg/RingyouC/orgImage_Size0256_lap128_ag_eq_rtt/"
# trainRatio,validRatio,testRatio = 0.7, 0.2, 0.1
# separateDatasetDir(inDir, trainRatio,validRatio, testRatio)
