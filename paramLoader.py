# coding: utf-8
import configparser
import errno
import shutil
import json
import configparser
import os



def getConfigValue(configPath:str):
    print("Setting Parameter File", configPath)
    # 指定したiniファイルが存在しない場合、エラー発生
    if not os.path.exists(configPath):
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), configPath)

    config_ini = configparser.ConfigParser()
    config_ini.read(configPath, encoding='utf-8')

    # --------------------------------------------------
    # config,iniから値取得
    # --------------------------------------------------

    BATCH_SIZE = int(config_ini['PARAMETER']['batchsize'])
    imgSize = int(config_ini['PARAMETER']['imgSize'])
    DATASET_DIR = str(config_ini['PARAMETER']['DATASET_DIR'])
    trainImgSize = int(config_ini['PARAMETER']["trainImgSize"])
    LR = float(config_ini['PARAMETER']['LR'])
    EPOCHS = int(config_ini['PARAMETER']['epoch'])

    return BATCH_SIZE, imgSize, trainImgSize, DATASET_DIR, LR, EPOCHS



# JusyuName={"スギ":"ceder","ヒノキ":"cypress","マツ":"matsu","その他針葉樹":"conifer","竹林":"bamboo","広葉樹":"hardwood","その他":"nonforest","ALL":"all"}
# JusyuName = {"針葉樹":"conifer","竹林":"bamboo","広葉樹":"hardwood","その他":"nonforest"}
 
def getJsonValue(jsonPath):
    Param_load = json.load(open(jsonPath, "r",encoding="utf-8"))
    Params = list(Param_load.values())


    JusyuStr = list(Params[0].keys())
    JusyuCode = list(Params[1].values())
    Rasvalue = list(Params[2].values())
    CLASSES = list(Params[0].values())
    
    dicelossWeights = Params[3]
    dataRatio = Params[4]
    return Rasvalue, JusyuStr, CLASSES, dicelossWeights, dataRatio, JusyuCode

def getJsonObj(jsonPath):
    Param_load = json.load(open(jsonPath, "r",encoding="utf-8"))
    return Param_load

