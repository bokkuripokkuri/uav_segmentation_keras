import numpy as np
# %matplotlib inline?#???????????????????
import matplotlib.pyplot as plt
import os
from PIL import Image
# import keras
# from keras.models import Model

# from keras.layers import Conv2D, MaxPooling2D, Input, Conv2DTranspose, Concatenate, BatchNormalization, UpSampling2D
# from keras.layers import  Dropout, Activation
# from keras.optimizers import Adam, SGD
# from keras.layers.advanced_activations import LeakyReLU
# from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau, EarlyStopping
# from keras import backend as K
# from keras.utils import plot_model

# import tensorflow as tf
import glob
import random
import cv2
from random import shuffle

#?????????
import pandas as pd

#???????? JST
from pytz import timezone
from datetime import datetime


import rasterio
import gdal

import subprocess
import geopandas as gpd

# GeoRaster Library rasterio
from rasterio.plot import show
from rasterio.plot import show_hist
from rasterio.mask import mask
from shapely.geometry import box
import geopandas as gpd
from fiona.crs import from_epsg
# import pycrs

import fiona
from osgeo import ogr

from rasterio.mask import mask
from shapely.geometry import box
import geopandas as gpd
from fiona.crs import from_epsg
# import pycrs

from rasterio.merge import merge

import ogr, os


from shapely.geometry import Point, MultiPolygon, Polygon 


import shutil


import os
import numpy as np

import matplotlib.pyplot as plt
import rasterio
import time
import glob

from tqdm import tqdm



workSpacePath = './'

for dirname in os.listdir("./"):
    if "driv" in dirname:
        workSpacePath = '/content/drive/My Drive/Colab Notebooks/ForestClass/'
        break
    else:
        workSpacePath = './'
        # workSpacePath = "G:/マイドライブ/Colab Notebooks/ForestClass/"

# print("workSpacePath:",workSpacePath)

def nowTimeStr():
  utc_now = datetime.now(timezone('UTC'))
#   print(utc_now)
  jst_now = utc_now.astimezone(timezone('Asia/Tokyo')).strftime("%Y%m%d_%H%M")
#   print(jst_now.strftime("%Y%m%d%H%M%S"))
#   id_str = str(id).zfill(3)
  return jst_now


def varPrint(varName):
  try:
    for (symbol, value) in globals().items():

      if symbol == varName:
        print('------------------')
        print('変数名は%s、値は%sです' % (symbol, value))
        print('------------------')
  except:
    print(varName, " 変数はありません。")

# 元画像のファイルパス をPILLOWで読込⇒Numpy形式⇒判定・PIL図表示用に変換して返す
def RawimgToNpimgForPrd(imgPath,imgSize):
    raw = Image.open(imgPath)
    raw = np.array(raw.resize(imgSize))/255
    return raw[:,:,0:3]


#元画像をModelで予測した結果を表示用に前処理を実施　マスク画像を白黒反転させて返す
def PrdForPlt(prdimg):
    prd  = prdimg.squeeze()
    prd = np.stack((prd,)*3, axis=-1)
    prd[prd >= 0.5] = 1 
    prd[prd < 0.5] = 0
    return prd

def MskimgtoNpimgForPlt(imgPath,imgSize):  
    msk = Image.open(imgPath)
    msk = np.array(msk.resize(imgSize))/255.#imgSize = (Width,Height)
    msk = 1-msk[:,:,0:3] #白黒変換
    return msk


#pltで4枚の画像を表示するときに境界線（赤色破線）を追加して表示する処理
def PltShowAddRedlineto4img(raw, prd, msk): 
    imgWigth = raw.shape[1]
    for i in range(len([raw, prd, raw* prd, msk])):

        start = [imgWidth * i, 0] #[x1,y1]
        end = [imgWidth * i, imgWidth] #[x2,y2]

    # 幅5pixelの赤線
    plt.plot( [start[0],end[0]], [start[1],end[1]], 'r--', lw=1 )

    #画像を表示  
    plt.plot( [0,imgWidth * len([raw, prd, raw* prd, msk])], [0, 0], 'r--', lw=1 )
    plt.plot( [0,imgWidth * len([raw, prd, raw* prd, msk])], [imgWidth, imgWidth], 'r--', lw=1 )
    plt.show()

  
# PIL画像を取得して表示する関数
def imgShowPlt(PILImage):
    #画像をarrayに変換
    PILImage_array = np.asarray(PILImage)
    #貼り付け
    plt.imshow(PILImage_array)
    #表示
    plt.show()

# PIL画像を指定の値でマスク画像(0, 255)をpilで作成する
def mskValuePil(msk,value):
    msk_np_temp = np.asarray(msk).copy()
    msk_np_temp[msk_np_temp!=value] = 255 
    msk_np_temp[msk_np_temp==value] = 0 
    msk_noforest = Image.fromarray(msk_np_temp)
    return msk_noforest

def saveModel(model):
    #モデルと重みを出力する
    model.save_weights(Model_PredictPath + nowTimeStr + 'unet_weights_' + ParamStr + '_rotated' + '.h5')
    model.save(Model_PredictPath + nowTimeStr +'unet_' + ParamStr + '_rotated' + '.hdf5')

    #モデルのアーキテクチャを保存 save as JSON
    json_string = model.to_json()
    open(os.path.join(Model_PredictPath, nowTimeStr + 'unet_model_' + ParamStr + '_rotated' + '.json'), 'w').write(json_string)
    print("モデルと重みを保存しました。", nowTimeStr, ParamStr)


#　ＰＩＬ画像のリストを受け取って表示する関数
def imgListShowPlt(PILImageList,imgSize):
    #show the msk and the segmented image
    
    PILImage_arrayList = []
    
    for PILImage in PILImageList:
        raw = np.array(PILImage.resize(imgSize))/255
        raw = raw[:,:,0:3]
        PILImage_arrayList.append(raw)

    combined = []
    combined = np.concatenate(PILImage_arrayList,axis = 1)

    fig, ax = plt.subplots()
    #表示
    plt.imshow(combined)
    #   plt.axis('off')
    plt.show()
    plt.close()




#　ＰＩＬ画像のファイルパスリストを受け取って表示する関数
def imgPathListShowPlt(PILImagePathList,imgSize):
    PILImageList = []
    for PILImagePath in PILImagePathList:
        PILImage = Image.open(PILImagePath)
        PILImageList.append(PILImage)
        
    imgListShowPlt(PILImageList,imgSize)


  #------rasterio デバック用プリント関数--------
def printIO(img_io):
    print("タイプ:",type(img_io))
    print("(高さ,幅)＝",img_io.shape)
    print("Band数:",img_io.count)    
    print("画像chのインデックス番号:",img_io.indexes)
    print("データセットのデータ型:",img_io.dtypes)    
    print("CRS:",img_io.crs)        
    print("mask:オブジェクトのType",type(img_io.dataset_mask()))
    print("画像の範囲:",img_io.bounds)
  




# http://pcjericks.github.io/py-gdalogr-cookbook/vector_layers.html?highlight=buffer#create-point-shapefile-with-attribute-data

import ogr, os

driver = ogr.GetDriverByName('ESRI Shapefile')
shpdriver = driver

def createBuffer(inputfn, outputBufferfn, bufferDist):
    inputds = ogr.Open(inputfn)
    inputlyr = inputds.GetLayer()

    shpdriver = ogr.GetDriverByName('ESRI Shapefile')
    if os.path.exists(outputBufferfn):
        shpdriver.DeleteDataSource(outputBufferfn)
    outputBufferds = shpdriver.CreateDataSource(outputBufferfn)
    bufferlyr = outputBufferds.CreateLayer(outputBufferfn, geom_type=ogr.wkbPolygon)
    featureDefn = bufferlyr.GetLayerDefn()

    for feature in inputlyr:
        ingeom = feature.GetGeometryRef()
        geomBuffer = ingeom.Buffer(bufferDist)

        outFeature = ogr.Feature(featureDefn)
        outFeature.SetGeometry(geomBuffer)
        bufferlyr.CreateFeature(outFeature)
        outFeature = None


import ogr, os

driver = ogr.GetDriverByName('ESRI Shapefile')
shpdriver = driver

    

def debugPrint(array):
    print("shape: ", array.shape)
    print("Min:  ",np.min(array),"\n",
          "Mean: ",np.mean(array),"\n",
          "Max:  ",np.max(array),"\n")

def createBuffer(inputfn, outputBufferfn, bufferDist):
    inputds = ogr.Open(inputfn)
    inputlyr = inputds.GetLayer()

    shpdriver = ogr.GetDriverByName('ESRI Shapefile')
    if os.path.exists(outputBufferfn):
        shpdriver.DeleteDataSource(outputBufferfn)
    outputBufferds = shpdriver.CreateDataSource(outputBufferfn)
    bufferlyr = outputBufferds.CreateLayer(outputBufferfn, geom_type=ogr.wkbPolygon)
    featureDefn = bufferlyr.GetLayerDefn()

    for feature in inputlyr:
        ingeom = feature.GetGeometryRef()
        geomBuffer = ingeom.Buffer(bufferDist)

        outFeature = ogr.Feature(featureDefn)
        outFeature.SetGeometry(geomBuffer)
        bufferlyr.CreateFeature(outFeature)
        outFeature = None

# フルパスと文字列を渡して、フルパス_文字列　のパスを作成
def new_Path(inPath,str_PlusWord):
    fileName, ext = os.path.splitext(os.path.basename(inPath))
    newPath = os.path.dirname(inPath) + "/" + fileName + "_" + str(str_PlusWord) + ext
    return newPath




# shpファイルを読み込んで、○○m のバッファを作成して保存　FIDしか残らない　createBuffer_prj(inputfn, bufferDist)

driver = ogr.GetDriverByName('ESRI Shapefile')
shpdriver = driver

def createBuffer(inputfn, outputBufferfn, bufferDist):
    inputds = ogr.Open(inputfn)
    inputlyr = inputds.GetLayer()

    shpdriver = ogr.GetDriverByName('ESRI Shapefile')
    if os.path.exists(outputBufferfn):
        shpdriver.DeleteDataSource(outputBufferfn)
    outputBufferds = shpdriver.CreateDataSource(outputBufferfn)
    bufferlyr = outputBufferds.CreateLayer(outputBufferfn, geom_type=ogr.wkbPolygon)
    featureDefn = bufferlyr.GetLayerDefn()

    for feature in inputlyr:
        ingeom = feature.GetGeometryRef()
        geomBuffer = ingeom.Buffer(bufferDist)

        outFeature = ogr.Feature(featureDefn)
        outFeature.SetGeometry(geomBuffer)
        bufferlyr.CreateFeature(outFeature)
        outFeature = None

from osgeo import ogr, osr

def getProjection(inputfn):
    driver = ogr.GetDriverByName('ESRI Shapefile')
    dataset = driver.Open(inputfn)
    layer = dataset.GetLayer()
    spatialRef = layer.GetSpatialRef()
    return spatialRef


def setProjection(inputfn,outputBufferfn,outEPSG):
    # input SpatialReference
    inSpatialRef = getProjection(inputfn)

    # output SpatialReference
    outSpatialRef = osr.SpatialReference()
    outSpatialRef.ImportFromEPSG(outEPSG)

    # create the CoordinateTransformation
    coordTrans = osr.CoordinateTransformation(inSpatialRef, outSpatialRef)

    # get the input layer
    inDataSet = driver.Open(inputfn)
    inLayer = inDataSet.GetLayer()

    # create the output layer
    outputShapefile = outputBufferfn
    if os.path.exists(outputShapefile):
        driver.DeleteDataSource(outputShapefile)
    outDataSet = driver.CreateDataSource(outputShapefile)
    outLayer = outDataSet.CreateLayer(os.path.basename(outputShapefile)[:-4], geom_type=ogr.wkbMultiPolygon)

def makePrjFile(inputfn,outputBufferfn):
    spatialRef = getProjection(inputfn)

    spatialRef.MorphToESRI()
    file = open(outputBufferfn[:-4] + ".prj", 'w')
    file.write(spatialRef.ExportToWkt())
    file.close()

def addAttribute(PolygonPath,AttributeStr,dict_Values):
    gdf_Shp = gpd.read_file(PolygonPath)
    # gdf_rinsouShp.plot()
    # print(gdf_rinsouShp.columns)
    # print(gdf_rinsouShp["樹種"])
    # print(gdf_rinsouShp["樹種名"])

    # 属性追加
    if  type(list(dict_Values.values())[0]) is str:
        gdf_Shp[AttributeStr] = ""
    elif type(list(dict_Values.values())[0]) is int:
        gdf_Shp[AttributeStr] = 0


    for key in list(dict_Values.keys()):
        for idx in range(len(gdf_Shp.index)):
            if gdf_Shp["樹種名"].iloc[idx] == key:
                gdf_Shp["Classvalue"].iloc[idx] = dict_Values[key]
                gdf_Shp["Classname"].iloc[idx] = dict_Values[key]            
                gdf_Shp["Classindex"] = idx + 1
                print(key,"Classvalue",dict_Values[key])

    del gdf_Shp["樹種名"]
    del gdf_Shp["樹種"]
    del gdf_Shp["面積ha"]



def imgChk(imgPath):
    imgPil = Image.open(imgPath)
    print("Pillow:",np.mean(np.asarray(imgPil)))

    imgCv2 = cv2.imread(imgPath)
    print(np.mean(imgCv2))
    plt.imshow(imgCv2)

# plt.imshow(cv2.imread(imgs[20]))



def getImgCropInfo(imgPil,imgWidth, SlideSize):
    
    width, height = imgPil.size
    print("Original Size:",imgWidth, SlideSize)

    #画像サイズとクリップサイズで切りがいい画素サイズにリサイズする。
    #   print(org.size, "リサイズ前")
    if SlideSize==0:
        resizeForSlideSize = (width - (width % imgWidth), height - (height % imgWidth))
        #スライドサイズに応じて、元画像の縦横幅から、クロッピング数を計算
        xl = width // imgWidth
        yl = height// imgWidth
    else:
        resizeForSlideSize = (width - (width % (imgWidth - SlideSize)), height - (height % (imgWidth - SlideSize)))
        #スライドサイズに応じて、元画像の縦横幅から、クロッピング数を計算
        xl = (width - (imgWidth - SlideSize))// SlideSize
        yl = (height - (imgWidth - SlideSize))// SlideSize

    print("Resize for Crop",resizeForSlideSize)

    print("水平方向 xl: " + str(xl) + "  垂直方向 yl " + str(yl))

#     print(xl,yl)
    return resizeForSlideSize, xl,yl


def devideImg(imgPath, cropNum, SlideNum, saveDir):
    img = Image.open(imgPath.replace("\\","/"))
    img_np = np.array(img)

    height, width = img.size

    noNataRatio = 0.3

    
    imgSize = height//cropNum
    SlideSize = imgSize//SlideNum

    # print("height, width, ch", h, w, ch)
    # print("imgSize, SlideSize", imgSize, SlideSize)

    xl = (width - (imgSize - SlideSize))// SlideSize
    yl = (height - (imgSize - SlideSize))// SlideSize

    # print(xl,yl)

    divideImgs = {}

    for x in range (xl):
        for y in range(yl):


            box = (x*SlideSize,y*SlideSize,x*SlideSize+imgSize,y*SlideSize+imgSize)

            img_crop = img.crop(box)
            cropImg = np.array(img_crop)

            if not ckimgNpNanData(cropImg,noNataRatio):
                # print("   Img NoData Contans",str(noNataRatio*100)+"% \t",saveImgPath)
                pass

            else:
                crop = Image.fromarray(cropImg)
                saveFileName = os.path.basename(imgPath).split(".")[0] + "_x_" + str(x).zfill(len(str(imgSize))) + "_y" + str(y).zfill(len(str(imgSize))) + "." + os.path.basename(imgPath).split(".")[-1]
                # if not os.path.exists(saveDir + saveFileName):
                # crop.save(saveDir + saveFileName)
                divideImgs[saveDir + saveFileName]=crop
    return divideImgs

# gdal　rasterioで保存したtifファイルは、そのままでは　Pil　Opencvでは読み込めない
# tif→png変換　msk画像のみ
def gdalTifTransPng(imgPath):

    options_list = ['-ot Byte',
                    '-of PNG',
                    '-b 1',
                    # '-scale'
                    ]
                    
    options_string = " ".join(options_list)

    savePngPath=imgPath[:-4]+".png"

    gdal.Translate(savePngPath,imgPath,options=options_string)
    # imgChk("test.png")
    return savePngPath


def Vector2Raster(inShpPath, outRasterAreaPath, outRasterPath, cellsize, EPSGcode, field_name=False, NoData_value=-9999):

    input_shp, area_shp, output_tiff = inShpPath, outRasterAreaPath, outRasterPath

    print("Reading  入力図形")
    readTest = gpd.read_file(input_shp)
    readTest.plot()

    # Shpファイルのドライバー
    inp_driver = ogr.GetDriverByName('ESRI Shapefile')

    print("Reading  空間参照")
    inp_source = inp_driver.Open(input_shp, 0)
    inp_lyr = inp_source.GetLayer()
    inp_srs = inp_lyr.GetSpatialRef()
    print("　入力図形の空間参照:",inp_srs,"\n")
    print("　書き込むの空間参照:",inp_srs.ExportToWkt(),"\n")

    print("Reading　図形範囲の参照:",area_shp)
    area_source = inp_driver.Open(area_shp, 0)
    area_lyr = inp_source.GetLayer()
    area_srs = inp_lyr.GetSpatialRef()


    # Extent 範囲
    x_min, x_max, y_min, y_max = area_lyr.GetExtent()

    # 縦横のピクセル数
    x_ncells = int((x_max - x_min) / cellsize)
    y_ncells = int((y_max - y_min) / cellsize)

    # Output　出力する画像
    print("Making OutputRaster")
    out_driver = gdal.GetDriverByName('GTiff')
    if os.path.exists(output_tiff):
        out_driver.Delete(output_tiff)
    out_source = out_driver.Create(output_tiff, x_ncells, y_ncells, 1, gdal.GDT_Int16)

    print("ProjectionGet OutputRaster")
    # srs = osr.SpatialReference() #空間参照情報のライブラリ群
    inp_srs.ImportFromEPSG(EPSGcode) #空間参照をEPSGcodeに設定
    out_source.SetProjection(inp_srs.ExportToWkt())

    print("AreaGet OutputRaster")
    out_source.SetGeoTransform((x_min, cellsize, 0, y_max, 0, -cellsize))



    out_lyr = out_source.GetRasterBand(1)
    out_lyr.SetNoDataValue(NoData_value)

    print("Value Writing OutputRaster")
    # Rasterize
    if field_name:
        gdal.RasterizeLayer(out_source, [1], inp_lyr,
                            options=["ATTRIBUTE={0}".format(field_name)])
    else:
        gdal.RasterizeLayer(out_source, [1], inp_lyr, burn_values=[1])

    # Save and/or close the data sources
    inp_source = None
    out_source = None

    # Return
    return output_tiff 



# # バックアップをとり、コピー先のファイルパスを返す
def backupShpData(inShpPath):
    print("backup!")
    try:
        input_shp_setPaths = glob.glob(inShpPath[:-4]+".*")
        print(len(input_shp_setPaths))
        if len(input_shp_setPaths)==0:
            print("shpファイルセットを取得できませんでした.\n",inShpPath[:-4]+".*")
        for inputShpsetPath in input_shp_setPaths:
            print(inputShpsetPath)
            outShpPath = workSpacePath + "/Unet_Workspace/00_org_TrainingData/Polygon/" + os.path.basename(inputShpsetPath)
            shutil.copyfile(inputShpsetPath, outShpPath)
            print("コピーしました.")
    except:
        print(sys.exc_info())
        print("入力ファイルが間違っています。\n",inShpPath,"\n")
        outShpPath = inShpPath
        pass
    return outShpPath


def createBuffer_prj(inputfn, bufferDist):
    outpath = os.path.dirname(inputfn) + '/buffer_' + str(bufferDist) + '/'
    os.makedirs(outpath,exist_ok=True)
    outputBufferfn = new_Path(outpath + os.path.basename(inputfn),"buffer" + str(bufferDist) +"m")
    print(outputBufferfn)

    createBuffer(inputfn, outputBufferfn, bufferDist)

    makePrjFile(inputfn,outputBufferfn)
    # setProjection(inputfn,outputBufferfn,2445)
    return outputBufferfn

def gdfDissolve2Buffer(inShpPath, dissolveFieldName, bufferDistance):
    gdf_org = gpd.read_file(inShpPath, encoding="UTF-8")

    print("カラムリスト", list(gdf_org.columns),gdf_org.columns[0])
    # カラムリスト Index(['Classvalue', 'id', 'area_ha', 'CityID', 'DEM_center', 'geometry'], dtype='object')

    if not dissolveFieldName==gdf_org.columns[1]:
        print(dissolveFieldName, "ディゾルブ用のキーではありません")
        pass

    gdf_org_disso =gdf_org.dissolve(by=gdf_org.columns[0], as_index=False)

    print(dissolveFieldName)

    gdf_org_dissoPath = new_Path(inShpPath,"dissolved_" + dissolveFieldName)
    gdf_org_disso.to_file(gdf_org_dissoPath)

    # shpファイルで出力される
    dissolveBufferdPath = createBuffer_prj(gdf_org_dissoPath,bufferDistance)

    return dissolveBufferdPath


# rasterio mask用に　　geopandas を　geometry 形式を変更
def getFeatures(gdf,n):
    """Function to parse features from GeoDataFrame in such a manner that rasterio wants them"""
    import json
    return [json.loads(gdf.to_json())['features'][n]['geometry']]

from tqdm import tqdm



def cropRaster(inputRasterPath, cropShptile, cropImageDir):
    baseImgPath = inputRasterPath

    os.makedirs(cropImageDir,exist_ok=True)

    if not os.path.exists(baseImgPath):
        print("InputImage Not Find")
        exit()

    with rasterio.open(baseImgPath) as img:
    

        out_meta = img.meta

        baseShpPath_tile = gpd.read_file(cropShptile)

        # 切り抜くタイル画像をひとつづつ取り出す
        for i in tqdm(range(len(baseShpPath_tile.index))):
            try:
                print("\n\n")
                saveImgPath = cropImageDir + str(i).zfill(10) + "_" + os.path.basename(cropShptile)[:-4] + ".tif" 

                # 既にクリップ済みデータは、処理しない
                if os.path.exists(saveImgPath):
                    continue

                print("\n",i,"/", len(baseShpPath_tile.index))

                cropShp = getFeatures(baseShpPath_tile,i)

                print("cropShp.bounds:",cropShp)

                # try:
                clipedImgNp, clip_transform = mask(dataset=img, shapes=cropShp, crop=True, filled = False)
                # except:
                #     print(i, " Input shapes do not overlap raster")
                    # continue
                # print(type(clipedImgNp),clipedImgNp.shape)

                print("np.mean(clipedImgNp):",np.mean(clipedImgNp), "\t",clipedImgNp.shape )

                chNum, height,width = clipedImgNp.shape
                
                
                if np.mean(clipedImgNp)==0:
                    print("   Not have Data",saveImgPath)
                    continue

                noNataRatio = 0.2
                if not ckimgNpNanData(clipedImgNp,noNataRatio):
                    print("   NoData Contans",str(noNataRatio*100)+"% \t",saveImgPath)
                    continue

                if chNum == 1:
                    orgPath = saveImgPath.replace("orgImage","mskImage")
                    if not os.path.basename(orgPath) == os.path.basename(saveImgPath):
                        os.remove(orgPath)
                        continue


                if not int(width/100) == int(height/100):
                    print("   height-width is not matching", width, height, saveImgPath)                    
                    continue


                out_meta.update({"driver": "GTiff",
                                "height": height,
                                "width": width,
                                'crs': img.crs,
                                "transform": clip_transform})

                os.makedirs(cropImageDir,exist_ok=True)
                with rasterio.open(saveImgPath, "w", **out_meta) as dest:
                    dest.write(clipedImgNp)
                    print("save ",saveImgPath)

                    # print("Cropped!" , saveImgPath)


            # except:
            #     # クリップ範囲が画像の範囲外であれば、Value Errorになる。        

            # except Exception as err:
            #     print("\n Error: {0}".format(err))
            #     print("Unexpected error:", sys.exc_info()[0])
            #     print("×× Can't　Cliping")
            #     pass
            except ValueError:
                print("\nValueError: Input shapes do not overlap raster..")
                print("×× Can't　Cliping")
                # pass
            # except:
            # print("Error:", sys.exc_info()[0])
            # print("×× Can't　Cliping")
            # raise




# shpファイルパスのリスト　→　Merge後のGdf
def shpMerge(shpPathList):

    BrowsringImageAreaPaths = shpPathList 
    # 処理したいshapeファイルのリスト
    shpfiles = [shpfile for shpfile in BrowsringImageAreaPaths]

    # shapeファイルを読み込んで結合させる  pandasでconcatしても、geopandasのgeodataframeになる。
    gdf_BImgArea = pd.concat([gpd.read_file(shpfile, encoding='shift-jis') for shpfile in shpfiles])

    # インデックスをリセットする
    gdf_BImgArea = gdf_BImgArea.reset_index()
    return gdf_BImgArea



# 画像を読み取り、画像範囲のPolygonオブジェクトを作成
def makeImgPoly(geoRasterPath):
    imgio = rasterio.open(geoRasterPath)
    imgLeft,imgBottom,imgRight,imgTop = imgio.bounds[0],imgio.bounds[1],imgio.bounds[2],imgio.bounds[3]

    del imgio

    # ポリゴンの頂点を指定　正方形なので4点
    coordinates = [(imgLeft,imgBottom),(imgRight,imgBottom),(imgRight,imgTop),(imgLeft,imgTop)]
    imgPoly = Polygon(coordinates)
    return imgPoly

# Clipレイヤと画像を読み込んで出力
def clipTif(orgTifFilename, clipShpName, outFileName):
    try:

        with fiona.open(clipShpName, "r") as shapefile:
            print("Opened!",clipShpName)
            shapes = [feature["geometry"] for feature in shapefile]
            # print(shapes)

        with rasterio.open(orgTifFilename) as img_io:
            print("Opened!",orgTifFilename)
            out_image, clip_transform = rasterio.mask.mask(img_io, shapes, crop=True)
            out_meta = img_io.meta
            print("Making Metadata")            

        out_meta.update({"driver": "GTiff",
                        "height": out_image.shape[1],
                        "width": out_image.shape[2],
                        "transform": clip_transform})

        with rasterio.open(outFileName, "w", **out_meta) as outimg_io:
            outimg_io.write(out_image)
        print("●● Clipped :  ",outFileName)

    except:
        print("×× Can't　Cliping")
        # クリップ範囲が画像の範囲外であれば、Value Errorになる。


import numpy as np
import rasterio
from rasterio.warp import calculate_default_transform, reproject, Resampling


def rePrjRaster(inputRasterPath, epsgCode):
    dst_crs = 'EPSG:' + str(epsgCode)

    with rasterio.open(inputRasterPath) as src:
        transform, width, height = calculate_default_transform(src.crs, dst_crs, src.width, src.height, *src.bounds)
        kwargs = src.meta.copy()
        kwargs.update({
            'crs': dst_crs,
            'transform': transform,
            'width': width,
            'height': height
        })

        rePrjRasterPath = os.path.dirname(os.path.dirname(inputRasterPath)) + "/Temp_ReprojectImage/" + os.path.basename(inputRasterPath)[:-4] + '_' + str(epsgCode) + '.tif'
        os.makedirs(os.path.dirname(rePrjRasterPath),exist_ok=True)
        with rasterio.open(rePrjRasterPath, 'w', **kwargs) as dst:
            for i in range(1, src.count + 1):
                reproject(
                    source=rasterio.band(src, i),
                    destination=rasterio.band(dst, i),
                    src_transform=src.transform,
                    src_crs=src.crs,
                    dst_transform=transform,
                    dst_crs=dst_crs,
                    resampling=Resampling.nearest)
    return rePrjRasterPath   


def imgSizeCheck(inImgPath, ckImgPath):

    ckResult = False

    with rasterio.open(inImgPath) as inImg:

        with rasterio.open(ckImgPath) as ckImg:

            if inImg.shape==ckImg.shape:
                ckResult = True

                # if inImg.bounds==ckImg.bounds:
                #     ckResult = True

    return ckResult


def ckimgNpNanData(img_io_np,ratio):
    ckBoole = False 

    if len(img_io_np.shape)==3:
        img_io_np = img_io_np[:,:,0]

    img_io_np = img_io_np.flatten()
    noZeros = np.count_nonzero(img_io_np)

    zeroRatio = 1 - noZeros / len(img_io_np)
    if zeroRatio < ratio:
        ckBoole = True
        print("NodataRatio:",zeroRatio)
    return ckBoole

def delImgNoDataDir(deleteDir:str):
    # deleteDir = "./02_Remake_TrainingData/ALL_000_128/orgImage_indexColor/"
    mskDir = deleteDir.replace("org","msk")

    delFilePaths = glob.glob(deleteDir + "*.*")

    delFilePaths.sort()

    for fPath_org in tqdm(delFilePaths):
        try:
            # fPath_org = fPath_msk.replace("msk","org").replace(".png",".jpg")
            fPath_msk = fPath_org.replace("org","msk").replace(".jpg",".png")

            checkImg = np.array(Image.open(fPath_org))

            delimgDataRatio = 0.2
            if mf.ckimgNpNanData(checkImg,delimgDataRatio):
                # os.remove(fPath_msk)
                # if os.path.exists(fPath_org):    
                #     os.remove(fPath_org)
                os.remove(fPath_org)
                if os.path.exists(fPath_msk):    
                    os.remove(fPath_msk)
        except:
            print("\n Error Not Delete \n", fPath_org,"\n",fPath_msk,"\n")



# 変数一覧

# treeType = "Mousou"
# memo = ""
# DatasetName =""


# nowTimeStr

# BrowsringImageArea_2445Dir = workSpacePath + "sateliteImages/03_Retile/BrowsringImageArea_2445/"

# BrowsringImageAreaPaths = glob.glob(BrowsringImageArea_2445Dir + "*.shp")

# mousouTiffPath

# mousouShp_SelectShpPath

# mousou_msk

# gdf_mousouTile

# gdf_BImgArea

# ClipImgArea



# global Calctime = 0 

#------デバック用プリント関数--------
def printIO(img_io):
    print("タイプ:",type(img_io))
    print("(高さ,幅)＝",img_io.shape)
    print("Band数:",img_io.count)    
    print("画像chのインデックス番号:",img_io.indexes)
    print("データセットのデータ型:",img_io.dtypes)    
    print("CRS:",img_io.crs)        
    print("mask:オブジェクトのType",type(img_io.dataset_mask()))
    print("画像の範囲:",img_io.bounds)
    
def printIO_Ch(img_io,index):
    img_ch = img_io.read(index)
    print("タイプ:",type(img_ch))
    print("(高さ,幅)＝",img_ch.shape)
#     print("Band数:",img_io.count)    
#     print("データセットのデータ型:",img_ch.dtypes)    
#     print("CRS:",img_ch.crs)        
#     print("画像の範囲:",img_ch.bounds)

def npPrint(ndarray):
  print("次元:",ndarray.shape)
  print("型:",type(ndarray))
  print("最大値:",ndarray.max())
  print("平均値:",ndarray.mean())
  print("最小値:",ndarray.min())
  print("--------------------\n")
#--------------------------------------


#--------------------------------------

from functools import wraps
import time
calcTime = []
# import SAR_remosen_DiffImage.Calctime as Calctime

# Calctime = 0

def stop_watch(func):
    @wraps(func)
    def wrapper(*args, **kargs) :
        start = time.time()
        result = func(*args,**kargs)
        elapsed_time =  time.time() - start
        elapsed_time = int(elapsed_time)
        # Calctime += elapsed_time
        print(f"    {func.__name__}は {elapsed_time}秒  かかりました")
        calcTime.append(elapsed_time)
        print("経過時間:",sum(calcTime)//60,"分")
        return result
    return wrapper






#------可視化関数--------
# ndarrayを渡してヒストグラムを表示
def showHist(ndarray):
  # (NumPyでヒストグラムの算出)
  hist, bins = np.histogram(ndarray.ravel(),int(ndarray.max()) ,[0,int(ndarray.max()) ])

  # ヒストグラムの中身表示
  # print(hist)

  # グラフの作成
  plt.xlim(-1 * int(ndarray.max()+1), int(ndarray.max()+1))
  plt.plot(hist)
  plt.xlabel("Pixel value")
  plt.ylabel("Number of pixels")
  # plt.xlabel("Pixel value", fontsize=20)
  # plt.ylabel("Number of pixels", fontsize=20)
  plt.grid()
  plt.show()

# ndarrayを渡して画像を表示+保存
def showImg(ndarray,savePath):
  plt.imshow(ndarray,cmap = "gray")
  #表示
  plt.show()
  cv.imwrite(savePath,ndarray)
  plt.close()

#---------------------




# rastaeioデータセットから、Nodata部分を取り出し、Nanとしたレイヤを取得する関数
@stop_watch
def getNodata2nan(img_io):
    # read_masks処理で　Band1からNodataのMaskを抽出
    msk_nodata = img_io.read_masks(1)
    
    # 全チャンネルをチェックして、Nodata位置を統合
    for ch in img_io.indexes:
        msk_ch = img_io.read_masks(ch)
        msk_nodata = msk_nodata * msk_ch
    
    # read_masks処理で抽出した、値0を Numpy.nanに置換　値があるピクセルは 1 
    msk_nan = np.where(msk_nodata == 0, np.nan, 1)
    return msk_nan


# rastaeioデータセットから単Bandを取り出した画像を正規化して返す。
# 正規化関数
#  返り値
##  nparray_Normalized: 全画素数の画素値の最大画素値を1 最小画素値を0としたnumpy.arrayオブジェクト
@stop_watch
def npToNormalize(nparray):

    # 画素値の値がない部分は　0　と仮定し
    npMin = np.nanmin(nparray[nparray!=0])
    npMax = np.nanmax(nparray[nparray!=0])
    print("元の画素値の Max：",npMax,", Min：",npMin )
    nparray_Normalized = (nparray - npMin)/(npMax - npMin)
    # nparray_Normalized ＝＝ np.ndarray　になる
    print("正規化後の画素値の Max：",np.amax(nparray_Normalized),", Min：",np.amax(nparray_Normalized) )
    print("正規化しました.")
    return nparray_Normalized


#量子化(N bit)化関数　 
#  返り値
##  np_N_floor: N bit化に量子化されたnumpy.arrayオブジェクト
@stop_watch
def npToNbit(nparray, Nbit):
    np_N = nparray
    if Nbit==8:
        np_N.astype(np.uint8)
    if Nbit==16:
        np_N.astype(np.uint16)
    if Nbit==32:
        np_N.astype(np.uint32)
    if Nbit==64:
        np_N.astype(np.uint64)

    np_N = nparray * ((2 ** Nbit) - 1)

    np_N.astype(np.float16)
    # print(Nbit,"bitへ量子化後の画素値の Max：",np.nanmax(np_N),", Min：",np.nanmin(np_N) )
    # print("量子化しました.")
    return np_N



# 累積分布関数　の計算 
#  返り値
##  vals: ユニークな画素値
##  ecdf: ユニークな画素値の確率　
# 　 画素値が100の場合、確率ecdf ＝ 100以下の画素値を持つ画素数 / 全画素数 
@stop_watch
def ecdf(nparray):
    # 「画素値のユニークな値」と「ユニークな値の個数」をそれぞれリストで取得
    vals, counts = np.unique(nparray, return_counts=True)
    # 「ユニークな値の個数」リストを始めから順に足し合わせていくcumsum　
    # 最終値は値の合計になり、確率分布計算の分母となる
    ecdf= np.cumsum(counts).astype(np.float32)
    # 個別の累積個数 / 個数合計 = 確率分布
    ecdf/= ecdf[-1]
    return vals, ecdf



@stop_watch
def releaceMemory(savePath,npyFileName,varObject):
    os.makedirs(savePath, exist_ok=True)
    np.save(savePath + npyFileName + ".npy", varObject)
    # if not os.path.isfile(savePath + npyFileName + ".npy"):
    #     np.save(savePath + npyFileName + ".npy", varObject)
    #     print("保存しました！", savePath + npyFileName + ".npy")
    # print("保存済み!",savePath + npyFileName + ".npy")    
    del(varObject)

# rasterioオブジェクトから、バンドの数だけnparray抽出して、npyファイルに出力する
@stop_watch
def dampIo2Npy(savePath, img_io, dict_ChannelKeyName):
    for i in tqdm(img_io.indexes):
        if not os.path.isfile(savePath + dict_ChannelKeyName[i] + ".npy"):
            print("Chanel:",i,"を読み込んでいます",dict_ChannelKeyName[i])
            picupChanel = img_io.read(i)
            releaceMemory(savePath, dict_ChannelKeyName[i], picupChanel)
            del(picupChanel)
        else:
            print("damp済み.",savePath + dict_ChannelKeyName[i] + ".npy")



# rasterioオブジェクトから、バンドの数だけnparray抽出して辞書型で返す
@stop_watch
def getIo2NpDict(img_io, dict_ChannelKeyName):
    bandNpArrays_dict = {}
    for i in tqdm(img_io.indexes):
        # print("Chanel:",i,"を読み込んでいます",dict_ChannelKeyName[i])
        picupChanel = img_io.read(i)
        bandNpArrays_dict[dict_ChannelKeyName[i]] = picupChanel
    return bandNpArrays_dict



#‘‘‘‘‘‘‘‘‘‘ SAR画像の前処理関数‘‘‘‘‘‘‘‘‘‘

# @stop_watch
def calcImgEcdf(Band_nparray):
    print("計算中　正規化 → 16bit化")
    img_norm_16bit = npToNbit(npToNormalize(Band_nparray),16)
    # 累積分布計算
    print("計算中　累積分布")
    pixelValueList, probabilityList = ecdf(img_norm_16bit)
    return img_norm_16bit, pixelValueList, probabilityList




# ヒストグラムマッチング　nparrayオブジェクトから直接計算
@stop_watch
def HistMatch(img_before_norm_16bit,val_after, p_after, val_before, p_before):
    # val_after: 65499 個
    print("計算中　災害前 ヒストグラムマッチング")
    # 辞書オブジェクトdict型 key:value = 画素値：累積分布
    ##災害後
    ecdf_after_dict = dict(zip(val_after,p_after))
    ##災害前
    ecdf_before_dict = dict(zip(val_before,p_before))
    print("ecdf_after_dict 個数:",len(ecdf_after_dict.keys()))
    print("ecdf_before_dict 個数:",len(ecdf_before_dict.keys()))

    # 線形補間interp　
    # p_before - p_after　の関係式を使い、　
    # val_afterから、 val_before_changeを計算して取得する
    #　線形補間　災害後-災害前の累積分布が一致する画素値:val_before_changeを作成
    # 辞書オブジェクトdict型 key:value = 累積分布:画素値
    print("計算中　災害前 　　線形補間")
    val_before_change = np.interp(p_before, p_after, val_after)
    
    ##災害前　線形補間後
    ecdf_before_change_dict = dict(zip(p_before, val_before_change))
    print("ecdf_before_change_dict 個数:",len(ecdf_before_change_dict.keys()))


    # 画像の画素値を条件指定して置換する 　nparray[nparray==value]=changeValue
    img_before_HistMatch = img_before_norm_16bit.copy() #新たにメモリ確保　参照ではだめ
    print("計算中　災害前 　　画素値を置換")
    for p_before in tqdm(ecdf_before_change_dict.values()):
        try:
            v_before = ecdf_before_dict[p_before]
            img_before_HistMatch[img_before_HistMatch==v_before] = ecdf_before_change_dict[p_before]
            # print("changed:",ecdf_before_change_dict[p_before],"  from",v_before)
        except KeyError:
            # val_keyがない場合もあるので、エラー処理
            # print("Not change: ",v_before)
            pass

    # 置換後の値を65535で除算した0~1の実数値から 16bit化
    print("計算中　災害前 　　正規化 → 16bit化")
    img_before_HistMatch_norm_16bit = npToNbit(img_before_HistMatch/(2**16 - 1),16)
    return img_before_HistMatch_norm_16bit




# パッチ画像生成関数 
#  引数 　nparray画像、パッチウィンドウサイズ（pixel）　ストライドサイズ（pixel）
#  返り値
##  vals: 画素値
##  ecdf:  確率　 画素値が100の場合、確率ecdf ＝ 100以下の画素値を持つ画素数 / 全画素数 
from skimage.util.shape import view_as_windows

@stop_watch
def patchCalc(img_io_ch,patchSize,strideSize):
    patchWindow = (patchSize,patchSize)
    patchedImage = view_as_windows(img_io_ch, window_shape = patchWindow,step = strideSize)
    return patchedImage



@stop_watch
def patchCalcMean(img_io_ch,patchSize,strideSize):
    patchWindow = (patchSize,patchSize)
    patchedImage = view_as_windows(img_io_ch, window_shape = patchWindow,step = strideSize)
    # 画像の形状を変更 (row, width , patchrow, patchwidth) → (row ×width, patchrow, patchwidth)
    patchedImage_Re = patchedImage.reshape(-1 , patchSize, patchSize)


    return patchedImage_Re





# NDI変化画像生成関数  災害前後ペアのパッチ平均値で  −1≤𝑁𝐷𝐼𝑘≤1 を計算
@stop_watch
def ndi(afterImgArray, beforeImgArray):
    ndiImgArray = np.zeros(afterImgArray.shape)
    ndiImgArray = (afterImgArray - beforeImgArray) / (afterImgArray + beforeImgArray)
    return ndiImgArray


# @stop_watch
# def ndi(afterImgArray, beforeImgArray):
#     if not (afterImgArray==0 or beforeImgArray==0):    
#         ndiImgArray = (afterImgArray - beforeImgArray) / (afterImgArray + beforeImgArray)
#     else:
#         ndiImgArray = np.zeros(afterImgArray.shape)
#     return ndiImgArray





#読み込んだGeoTiffファイル(例：合成ファイル)のメタ情報を取得し、新規作成のGeoTiffに利用することで、読み込んだGeoTiffファイル(例：合成ファイル)と全く同じサイズのGeoTIFF(float32)を作成（読み込んだGeoTiffの画像データ部分のみを置き換える）
import gdal
gdal.PushErrorHandler('CPLQuietErrorHandler')

@stop_watch
def setGeometry2Np(RasterPath,setNumpy):
    ds = gdal.Open(RasterPath)
    band = ds.GetRasterBand(1)
    arr = band.ReadAsArray()
    [cols, rows] = arr.shape

    driver = gdal.GetDriverByName("GTiff")
    os.makedirs("./output/", exist_ok=True)
    outdata = driver.Create("./output/diddimage.tif", rows, cols, 1, gdal.GDT_Float32)
    outdata.SetDescription(ds.GetDescription())
    outdata.SetMetadata(ds.GetMetadata())
    outdata.SetGeoTransform(ds.GetGeoTransform())##sets same geotransform as input
    outdata.SetProjection(ds.GetProjection())##sets same projection as input
    outdata.GetRasterBand(1).WriteArray(setNumpy.astype(rasterio.float32))
    outdata.FlushCache() ##saves to disk!!
    del(outdata)
    del(band)
    del(ds)


def setGeometry2Np_outDir(RasterPath,setNumpy,outDir):
    ds = gdal.Open(RasterPath)
    band = ds.GetRasterBand(1)
    arr = band.ReadAsArray()
    [cols, rows] = arr.shape

    driver = gdal.GetDriverByName("GTiff")
    os.makedirs(outDir, exist_ok=True)
    outdata = driver.Create(outDir + os.path.basename(RasterPath), rows, cols, 1, gdal.GDT_Float32)
    outdata.SetDescription(ds.GetDescription())
    outdata.SetMetadata(ds.GetMetadata())
    outdata.SetGeoTransform(ds.GetGeoTransform())##sets same geotransform as input
    outdata.SetProjection(ds.GetProjection())##sets same projection as input
    outdata.GetRasterBand(1).WriteArray(setNumpy.astype(rasterio.float32))
    outdata.FlushCache() ##saves to disk!!
    del(outdata)
    del(band)
    del(ds)

def setGeometry2pil_outDir(RasterPath,img_pil,outDir):
    ds = gdal.Open(RasterPath)
    band = ds.GetRasterBand(1)
    arr = band.ReadAsArray()
    [cols, rows] = arr.shape

    img_pil = img_pil.resize([rows, cols])

        # img = img.resize(imgSize)
    img_np = np.array(img_pil)
    setNumpy = np.squeeze(img_np)

    driver = gdal.GetDriverByName("GTiff")
    os.makedirs(outDir, exist_ok=True)
    outdata = driver.Create(outDir + os.path.basename(RasterPath), rows, cols, 1, gdal.GDT_Float32)
    outdata.SetDescription(ds.GetDescription())
    outdata.SetMetadata(ds.GetMetadata())
    outdata.SetGeoTransform(ds.GetGeoTransform())##sets same geotransform as input
    outdata.SetProjection(ds.GetProjection())##sets same projection as input
    outdata.GetRasterBand(1).WriteArray(setNumpy.astype(rasterio.uint16))
    outdata.FlushCache() ##saves to disk!!


    del(arr)
    del(setNumpy)
    del(outdata)
    del(band)
    del(ds)


import rasterio
import numpy as np
import gdal
import os

def printIO(img_io):
    print("???:",type(img_io))
    print("(??,?)?",img_io.shape)
    print("Band?:",img_io.count)    
    print("??ch?????????:",img_io.indexes)
    print("???????????:",img_io.dtypes)    
    print("CRS:",img_io.crs)        
    print("?????:",img_io.bounds)

def npToNormalize(nparray):
    # print("Norm array.shape",nparray.shape)
    if not len(nparray)==0:
        # ????????????0?????
        npMin = np.nanmin(nparray[nparray!=0])
        npMax = np.nanmax(nparray[nparray!=0])
    else:
        npMin = 0
        npMax = 1

#     print("?????? Max?",npMax,", Min?",npMin )
    nparray_Normalized = (nparray - npMin)/(npMax - npMin)
    # nparray_Normalized ?? np.ndarray????
#     print("????????? Max?",np.amax(nparray_Normalized),", Min?",np.amax(nparray_Normalized) )
#     print("???????.")
    return nparray_Normalized

def npToNbit(nparray, Nbit):
    np_N = nparray
    if Nbit==8:
        np_N.astype(np.uint8)
    if Nbit==16:
        np_N.astype(np.uint16)
    if Nbit==32:
        np_N.astype(np.uint32)
    if Nbit==64:
        np_N.astype(np.uint64)

    np_N = nparray * ((2 ** Nbit) - 1)

    np_N.astype(np.float16)
#     print(Nbit,"bit?????????? Max?",np.nanmax(np_N),", Min?",np.nanmin(np_N) )
#     print("???????.")
    return np_N


def makeFalseNp(imagePath):

    # with rasterio.open(imagePath,"r") as img4band_io:
    img4band_io = rasterio.open(imagePath,"r")
    
    # printIO(img4band_io)

    falsedata = np.zeros(shape=([img4band_io.height,img4band_io.width,3])).astype(np.uint16)

    band4 = npToNbit(npToNormalize(img4band_io.read(4)),16).copy()
    band3 = npToNbit(npToNormalize(img4band_io.read(3)),16).copy()
    band2 = npToNbit(npToNormalize(img4band_io.read(2)),16).copy()
    # print("band4 read")

    falsedata[:,:,0] = band4
    falsedata[:,:,1] = band3
    falsedata[:,:,2] = band2

    return falsedata

def makeTrueColorNp(imagePath):

    # with rasterio.open(imagePath,"r") as img4band_io:
    img4band_io = rasterio.open(imagePath,"r")
    
    # printIO(img4band_io)

    truedata = np.zeros(shape=([img4band_io.height, img4band_io.width, 3])).astype(np.uint16)

    band4 = npToNbit(npToNormalize(img4band_io.read(3)), 8).copy()
    band3 = npToNbit(npToNormalize(img4band_io.read(2)), 8).copy()
    band2 = npToNbit(npToNormalize(img4band_io.read(1)), 8).copy()
    # print("band4 read")

    truedata[:,:,0] = band4
    truedata[:,:,1] = band3
    truedata[:,:,2] = band2

    return truedata

def make16bitTrueColorNp(imagePath):

    # with rasterio.open(imagePath,"r") as img4band_io:
    img4band_io = rasterio.open(imagePath,"r")
    
    # printIO(img4band_io)

    truedata = np.zeros(shape=([img4band_io.height, img4band_io.width, 3])).astype(np.uint16)

    band4 = npToNbit(npToNormalize(img4band_io.read(3)), 16).copy()
    band3 = npToNbit(npToNormalize(img4band_io.read(2)), 16).copy()
    band2 = npToNbit(npToNormalize(img4band_io.read(1)), 16).copy()
    # print("band4 read")

    truedata[:,:,0] = band4
    truedata[:,:,1] = band3
    truedata[:,:,2] = band2

    return truedata


def setGeometry2Np_false(RasterPath,setNumpy,outTifPath):
    ds_io = rasterio.open(RasterPath)
    bandcount = ds_io.count
    ds = gdal.Open(RasterPath)
    band = ds.GetRasterBand(1)
    arr = band.ReadAsArray()
    [cols, rows] = arr.shape

    driver = gdal.GetDriverByName("GTiff")
    os.makedirs(os.path.dirname(outTifPath), exist_ok=True)
    outdata = driver.Create(outTifPath, rows, cols, 3, gdal.GDT_UInt16)
    outdata.SetDescription(ds.GetDescription())
    outdata.SetMetadata(ds.GetMetadata())
    outdata.SetGeoTransform(ds.GetGeoTransform())##sets same geotransform as input
    outdata.SetProjection(ds.GetProjection())##sets same projection as input
    outdata.GetRasterBand(1).WriteArray(setNumpy[:,:,0].astype(rasterio.uint16))
    outdata.GetRasterBand(2).WriteArray(setNumpy[:,:,1].astype(rasterio.uint16))
    outdata.GetRasterBand(3).WriteArray(setNumpy[:,:,2].astype(rasterio.uint16))
    outdata.FlushCache() ##saves to disk!!
    del(outdata)
    del(band)
    del(ds)


def convFalseTif(inputTifPath, saveTifDir):
    falseNp = makeFalseNp(inputTifPath)
    # print(tifPath)
    outFalseDir = saveTifDir
    os.makedirs(outFalseDir, exist_ok=True)
    setGeometry2Np_false(inputTifPath, falseNp, outFalseDir + os.path.basename(inputTifPath))


def convFalse_TrueTif(inputTifPath, saveFalseTifDir, saveTrueTifDir):
    falseNp = makeFalseNp(inputTifPath)
    trueNp = makeTrueColorNp(inputTifPath)
    # print(tifPath)
    os.makedirs(saveFalseTifDir, exist_ok=True)
    os.makedirs(saveTrueTifDir, exist_ok=True)

    setGeometry2Np_false(inputTifPath, falseNp, saveFalseTifDir + os.path.basename(inputTifPath))

    setGeometry2Np_false(inputTifPath, trueNp, saveTrueTifDir + os.path.basename(inputTifPath))



import sys

# inputImgPath = sys.argv[1]

# command = "gdal_translate -b 2 -b 3 -b 4 -ot UInt16 -of GTiff " + inputImgPath + " " + inputImgPath[:-4] + "_False.tif"
# falseNp = makeFalseNp(inputImgPath)



# ヒストグラムマッチング後のオブジェクトから、災害前画像の数だけnparray抽出して辞書型で返す
@stop_watch
def getNpy2NpDict(npyPath, dict_ChannelKeyName):
    img_before_HistMatchPaths = glob.glob(npyPath + 'img_before*_HistMatch*.npy')
    bandNpArrays_dict = {}
    for img_before_HistMatchPath in tqdm(img_before_HistMatchPaths):
        # print("Chanel:",i,"を読み込んでいます",dict_ChannelKeyName[i])
        picupNpArray = np.load(img_before_HistMatchPath)
        bandNpArrays_dict[dict_ChannelKeyName[i]] = picupChanel
    return bandNpArrays_dict


import sys
# 利用メモリのチェック　変数
def printMemory():
    print("{}{: >25}{}{: >10}{}".format('|','Variable Name','|','Memory','|'))
    print(" ------------------------------------ ")
    for var_name in dir():
        if not var_name.startswith("_"):
            print("{}{: >25}{}{: >10}{}".format('|',var_name,'|',sys.getsizeof(eval(var_name)),'|'))



# ヒストグラムマッチング後のオブジェクトから、災害前画像の数だけnparray抽出して辞書型で返す
# @stop_watch
def getNpy2NpDict(npyPath):
    img_before_HistMatchPaths = glob.glob(npyPath + 'img_before*_HistMatch*.npy')
    bandNpArrays_dict = {}
    for img_before_HistMatchPath in tqdm(img_before_HistMatchPaths):
        keyName = os.path.basename(img_before_HistMatchPath)[:12]
        picupNpArray = np.load(img_before_HistMatchPath)
        bandNpArrays_dict[keyName] = picupNpArray
    return bandNpArrays_dict

def sarResize(nparray):
    print("リサイズ済：",(nparray.shape[0]//8, nparray.shape[1]//8))
    nparray_resized = nparray.copy().resize((nparray.shape[0]//8, nparray.shape[1]//8),refcheck=False)
    del(nparray)
    
    return np.array(nparray_resized)


# ヒストグラムマッチング後のオブジェクトから、災害前画像の数だけnparray抽出してPatch化と平均値計算したNpArrayを辞書型で返す
# @stop_watch
def getNpy2NpPatchMeanDict(npyPath):
    bandNpArrays_dict = {}
    print("災害前　patch画像/patch平均画像　作成中")
    img_before_HistMatchPaths = glob.glob(npyPath + 'img_before*_HistMatch*.npy')
    
    for img_before_HistMatchPath in tqdm(img_before_HistMatchPaths):
        img_before_HistMatchPath = img_before_HistMatchPath.replace("\\","/")
        keyName = os.path.basename(img_before_HistMatchPath)[:12]
        print("patch処理中",keyName)

        picupNpArray = np.load(img_before_HistMatchPath)
        print(picupNpArray.shape)
        img_before_HistMatch_patchs = patchCalc(picupNpArray, 5, 2)
        print(img_before_HistMatch_patchs.shape)
        patchsMean_before = np.nanmean(img_before_HistMatch_patchs, axis=(2,3))
        print(patchsMean_before.shape)
#         bandNpArrays_dict[keyName] = np.nanmean(patchCalc(np.load(img_before_HistMatchPath), 5, 2).astype(np.float32), axis=(2,3))
    return bandNpArrays_dict



  #------rasterio デバック用プリント関数--------
def printIO(img_io):
    print("タイプ:",type(img_io))
    print("(高さ,幅)＝",img_io.shape)
    print("Band数:",img_io.count)    
    print("画像chのインデックス番号:",img_io.indexes)
    print("データセットのデータ型:",img_io.dtypes)    
    print("CRS:",img_io.crs)        
    print("mask:オブジェクトのType",type(img_io.dataset_mask()))
    print("画像の範囲:",img_io.bounds)

def min_max(x, axis=None):
    min = x.min(axis=axis, keepdims=True)
    max = x.max(axis=axis, keepdims=True)
    result = (x-min)/(max-min)
    return result

def raster4BandCnvPillow(rasterioFilePath):
    with rasterio.open(rasterioFilePath) as im_io:

        ChCount = im_io.count

        bands3 = []
        for band in range(3):
            band = band + 1  
            im_io_arr = im_io.read(band)
            # im_io_arr.shape(h,w,ch)

            # im_io_arr_cnv = rasterio.plot.reshape_as_image(im_io_arr)
            im_io_arr_cnv = im_io_arr

            height, width=im_io_arr_cnv.shape

            im_io_arr_cnv = min_max(im_io_arr_cnv) * 255

            bands3.append(im_io_arr_cnv.reshape(height,width).copy())

        bands3 = np.array(bands3,dtype='int8').reshape(height, width, 3)
        
        img_cv_cnv = bands3
        # img_cv_cnv = cv2.cvtColor(bands3, cv2.COLOR_BGR2RGB)

        # Nodata -9999 Convert 0
        img_cv_cnv_nodata = np.where(img_cv_cnv==-9999,0,img_cv_cnv)

        img_pil = Image.fromarray(np.uint8(img_cv_cnv_nodata))

        return img_pil


Image.MAX_IMAGE_PIXELS = 100000000000


def rasterCnvPillow(rasterioFilePath):
    with rasterio.open(rasterioFilePath) as im_io:

        ChCount = im_io.count

        im_io_arr = im_io.read()
        # im_io_arr.shape(h,w,ch)

        im_io_arr_cnv = rasterio.plot.reshape_as_image(im_io_arr)

        height, width, ch =im_io_arr_cnv.shape

        if ChCount==1:
            # img_cv_cnv = cv2.cvtColor(im_io_arr_cnv, cv2.COLOR_BGR2GRAY)
            img_cv_cnv = im_io_arr_cnv.reshape(height,width).copy()
        else:
            img_cv_cnv = im_io_arr_cnv
            # img_cv_cnv = cv2.cvtColor(im_io_arr_cnv, cv2.COLOR_BGR2RGB)

        # img_cv_cnv = (npToNormalize(img_cv_cnv)*255).astype(np.uint8)

        # Nodata -9999 Convert 0
        img_cv_cnv_nodata = np.where(img_cv_cnv==-9999,0,img_cv_cnv)

        img_pil = Image.fromarray(np.uint8(img_cv_cnv_nodata))

        return img_pil


def convIndexColor_fromNp(array):
    paletteImg = Image.open(workSpacePath + "colorPalette.png")
    palette = paletteImg.getpalette()

    msk = Image.fromarray(array.astype(np.int8)).convert("P")

    msk.putpalette(palette)

    # msk.save(IndexImgPath)

    return msk
    # print("Saved!  ",IndexImgPath)



def convIndexColor(tifPath):
    paletteImg = Image.open(workSpacePath + "colorPalette.png")
    palette = paletteImg.getpalette()

    msk = rasterCnvPillow(tifPath)

    msk.putpalette(palette)

    # msk.save(IndexImgPath)

    return msk
    # print("Saved!  ",IndexImgPath)


def msk0Value(org_np, msk_np):

    # org = cv2.imread(orgPath)
    org = org_np
    # rgb_org = cv2.cvtColor(org, cv2.COLOR_BGR2RGB)

    msk = msk_np
    # msk = cv2.imread(mskPath)
    # msk = cv2.cvtColor(msk,cv2.COLOR_RGB2GRAY)
    threshold = 0

    msk_bin = np.where(msk > threshold,1,0)

    height, width, ch = org.shape # 幅・高さ・色を取得
    dst = np.zeros((height, width, ch), dtype = "uint8") # Msk合成画像用

    
    # for y in range(0, height):
    #     for x in range(0, width):
    #         if (msk[y][x] > threshold ):
    #             dst[y][x] = org[y][x] 
    #         else:
    #             dst[y][x] = 0 

    

    org_msked = org * np.stack([msk_bin]*3, axis=2)

    org_msked_pil = Image.fromarray(np.uint8(org_msked))

    return org_msked_pil